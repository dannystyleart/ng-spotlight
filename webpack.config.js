var webpack = require('webpack'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    path = require('path'),
    BowerWebpackPlugin = require("bower-webpack-plugin"),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    HtmlConfig = {
        title: 'AngularJS Webpack',
        template: './src/index.html.ejs',
        filename: '../index.html'
    },
    templatesRoot = path.resolve(__dirname, './src/');
module.exports = {
    entry: {
        spotlight: './src/scripts/spotlight.module.js',
        app: './src/scripts/app.js',
        vendor: './src/scripts/vendor.js'
    },
    output: {
        path: './dist/scripts',
        publicPath: 'scripts/',
        filename: '[name].js',
        minify:{
            html5: true,
            minifyCSS: true,
            maxLineLength: 160
        }
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel?compact=true'
            },
            {
                test: /\.html$/,
                loaders: ['ngtemplate?relativeTo=' + templatesRoot + '/',
                    'html',
                    'html-minify'
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file?name=../fonts/[name].[ext]'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader",{publicPath: './'})
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin("../styles/demo.css"),
        new HtmlWebpackPlugin(HtmlConfig),
        new BowerWebpackPlugin()
    ],
    devtool: 'eval-source-map'
};