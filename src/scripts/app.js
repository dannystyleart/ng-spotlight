require('../styles/demo.css');
let exampleApp = angular.module('ds.exampleApp', ['ds.spotlight']);

exampleApp.config(['$spotlightSettingsProvider', ($spotlightSettingsProvider) => {

    let actions = [
            $spotlightSettingsProvider.action('Yahoo', (action) => {
                alert(action.title + ' clicked!');
            }),
            $spotlightSettingsProvider.action('Sombrero', (action) => {
                alert(action.title + ' clicked!');
            }),
            $spotlightSettingsProvider.action('Brada', (action) => {
                alert(action.title + ' clicked!');
            }),
            $spotlightSettingsProvider.action('Hola', (action) => {
                alert(action.title + ' clicked!');
            })
        ];


    $spotlightSettingsProvider.setLoader((txt, cb) => {

        let search = txt.toLowerCase(),
            found = actions.filter(action => {
                return action.title.toLowerCase().indexOf(search) > -1;
            });


        cb(found);
    })
}]);

angular.element(document).ready(function () {
    angular.bootstrap(document, [exampleApp.name], {
        strictDi: false
    });
});