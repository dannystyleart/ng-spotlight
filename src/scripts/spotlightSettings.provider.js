class SpotlightAction {
    constructor( title, onClick = angular.noop ) {
        this.title = title;
        this.onClick = onClick.bind(this);
    }
}

export class SpotlightSettings {

    constructor() {
        this.activateKeyCode = 190;
        this.cancelKeyCode = 27;
        this.loaderFn = angular.noop;
    }

    setLoader( loaderFn ) {
        if ( !angular.isFunction(loaderFn) ) {
            throw Error('Spotlight::setActionLoader accepts loaderFn as a function but given' + loaderFn);
        }
        this.loaderFn = loaderFn;
    }

    setActivateKey( keyCode = 190 ) {
        if ( parseInt(keyCode) === NaN ) {
            throw Error('Spotlight::setActivateKey accepts keycode as a number but given' + keyCode);
            return;
        }
        this.activateKeyCode = keyCode;
    }

    setCancelKey( keyCode = 27 ) {
        if ( parseInt(keyCode) === NaN ) {
            throw Error('Spotlight::setCancelKey accepts keycode as a number but given' + keyCode);
            return;
        }
        this.cancelKeyCode = keyCode;
    }

    action( title, onClick ) {
        return new SpotlightAction(title, onClick);
    }

    $get() {

        return {
            ActivateKey: this.activateKeyCode,
            CancelKey: this.cancelKeyCode,
            Loader: this.loaderFn
        };

    }
}