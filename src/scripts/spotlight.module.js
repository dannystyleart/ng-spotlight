import { SpotlightSettings } from './spotlightSettings.provider';
import { SpotlightComponent } from './spotlight.component';

export let spotlightModule = angular.module('ds.spotlight', []);
spotlightModule.provider('$spotlightSettings', SpotlightSettings)
spotlightModule.component('spotlight', SpotlightComponent.create());