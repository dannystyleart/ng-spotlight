require('./spotlight.html');

const TIMINGS = {
    KEYUP_TIMEOUT: 300,
    INPUT_FOCUS: 100
};

export class SpotlightComponent {

    static create() {
        return {
            controller: SpotlightComponent,
            templateUrl: 'scripts/spotlight.html'
        }
    }

    constructor( $document, $timeout, $scope, $spotlightSettings ) {
        this.$document = $document;
        this.$timeout = $timeout;
        this.$scope = $scope;

        this.spotlightVisible = false;
        this.spotlightActions = [];
        this.spotlightSearch = '';
        this.KEY_UP_COUNT = 0;
        this.KEY_UP_PROMISE = undefined;

        this.LOADER = $spotlightSettings.Loader;
        this.ACTIVATE_KEY = $spotlightSettings.ActivateKey;
        this.CANCEL_KEY = $spotlightSettings.CancelKey;

        this.registerListeners();
    }

    /**
     * Handling keyup for activation key
     * @param {KeyboardEvent} e Jquery Keyboard Event
     */
    keyTapListener( e ) {
        let keyCode = e.keyCode;
        if ( keyCode === this.CANCEL_KEY && this.spotlightVisible ) {
            this.KEY_UP_COUNT = 0;
            this.closeSpotlight();
            this.$scope.$apply();
            return;
        }

        if ( keyCode !== this.ACTIVATE_KEY ) {
            this.KEY_UP_COUNT = 0;
            if ( angular.isDefined(this.KEY_UP_PROMISE) ) {
                this.$timeout.cancel(this.KEY_UP_PROMISE);
            }
        }

        if ( !this.spotlightVisible ) {
            this.KEY_UP_COUNT++;
            if ( angular.isDefined(this.KEY_UP_PROMISE) ) {
                this.$timeout.cancel(this.KEY_UP_PROMISE);
            }
            this.KEY_UP_PROMISE = this.$timeout(this.openSpotlight.bind(this), TIMINGS.KEYUP_TIMEOUT);
        }
    }

    /**
     * Handling click event outside spotlight
     * @param {ClickEvent} e Jquery Click Event
     */
    outsideClickListener( e ) {
        if ( !this.spotlightVisible ) {
            return;
        }

        let spotlighElem = this.$document[0].querySelectorAll('div.spotlight')[0],
            isInSpotlight = e.path.filter(step => step === spotlighElem).length > 0;

        if ( !isInSpotlight ) {
            e.preventDefault();
            this.closeSpotlight();
            this.$scope.$apply();
        }
    }

    registerListeners() {
        this.$document.bind('click', this.outsideClickListener.bind(this));
        this.$document.bind('keyup', this.keyTapListener.bind(this));
        this.$scope.$on('$destroy', () => {
            this.$document.unbind('click', this.outsideClickListener.bind(this));
            this.$document.unbind('keyup', this.keyTapListener.bind(this));
        });
    }

    closeSpotlight() {
        this.spotlightVisible = false;
        this.spotlightActions = [];
        this.spotlightSearch = '';
    }

    openSpotlight() {
        if ( this.KEY_UP_COUNT === 2 ) {
            this.spotlightVisible = true;
            this.KEY_UP_PROMISE = undefined;
            this.$timeout(this.focusInput.bind(this), TIMINGS.INPUT_FOCUS);
        }
        this.KEY_UP_COUNT = 0;
    }

    runAction( action ) {
        action.onClick.call(action, action);
        this.closeSpotlight();
    }

    onSearchUpdate() {
        if ( !this.spotlightSearch.trim().length ) {
            this.spotlightActions = [];
            return;
        }

        this.LOADER.call(null, this.spotlightSearch.trim(), this.setActions.bind(this));

    }

    setActions( actions = [] ) {
        if ( !angular.isArray(actions) ) {
            throw Error('Spotlight::Loader::Callback accepts only array as argument but given: ' + actions);
            this.spotlightActions = [];
            return;
        }
        this.spotlightActions = actions;
    }

    focusInput() {
        this.$document[0].querySelectorAll('.spotlight input[type="text"]')[0].focus();
    }

}
SpotlightComponent.$inject = ['$document', '$timeout', '$scope', '$spotlightSettings'];

