var webpack = require('webpack');
    config = require('./webpack.config');

var uglifier = new webpack.optimize.UglifyJsPlugin({
    sourceMap: false,
    mangle: false,
    minimize: true
});


config.plugins.push(uglifier);

delete config.devtool;
module.exports = config;